package main

import (
	"net/http"
	"ormahstitches/controllers"

	"github.com/gorilla/mux"
)

func main() {
	usersC := controllers.NewUsers()
	staticC := controllers.NewStatic()

	r := mux.NewRouter()
	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")
	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")

	// Startup a web server listening on port 3000
	_ = http.ListenAndServe(":3000", r)
	println("Server is up and listening on port 3000")
}
